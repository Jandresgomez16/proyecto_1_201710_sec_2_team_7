package view;

import java.util.Iterator;

import model.data_structures.ListaDE;
import model.logic.ServicioRecomendacionPeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;


public class Consola {
	private static final String PATH_MOVIES = "data/movies.csv";
	private static final String PATH_RATINGS = "data/ratings.csv";
	private static final String PATH_TAGS = "data/tags.csv";

	public static String toString(VOPelicula p) {
		String a = p.getIdPelicula() + ": " + p.getTitulo() + ", " + p.getAgnoPublicacion();
		a += "\n"; Iterator<String> generos = p.getGenerosAsociados().iterator();
		while(generos.hasNext()) { a+= generos.next(); a+= ", "; }
		return a.substring(0, a.length() - 2);
	}

	public static void genderCheck(ListaDE<VOGeneroPelicula> list) {
		Iterator<VOGeneroPelicula> itr = list.iterator();
		int count = 0;
		while(itr.hasNext()) {
			VOGeneroPelicula gp = itr.next();
			ListaDE<VOPelicula> ps = (ListaDE<VOPelicula>) gp.getPeliculas();
			String genero = gp.getGenero();
			Iterator<VOPelicula> itrPeliculas = ps.iterator();
			while(itrPeliculas.hasNext()) {
				VOPelicula p = itrPeliculas.next();
				Iterator<String> generos = p.getGenerosAsociados().iterator();
				boolean check = false;
				while(!check && generos.hasNext()) if(genero.compareToIgnoreCase(generos.next()) == 0) check = true;
				if(!check) count++;
			}
		} System.out.println(count == 0? "OK Split" : "NOT OK Split");
	}


	public static void main(String[] args) {
		ServicioRecomendacionPeliculas srp = new ServicioRecomendacionPeliculas();
		srp.cargarPeliculasSR(PATH_MOVIES);
		System.out.println("movies");
		srp.cargarRatingsSR(PATH_RATINGS);
		System.out.println("ratings");
		srp.cargarTagsSR(PATH_TAGS);
		System.out.println("tags");
		ListaDE<VOGeneroPelicula> lgp = srp.splitGeneros(srp.peliculas);
		genderCheck(lgp);
	}
}
