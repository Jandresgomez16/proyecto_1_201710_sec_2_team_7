package model.data_structures;

public class Queue<T> {
	private ListaE tasks;
	
	public Queue() {
		tasks = new ListaE();
	}
	
	public void enqueue(T e) {
		tasks.agregarElementoFinal(e);
	}
	
	public T dequeue() {
		return (T) tasks.popFirst();
	}
	
	public boolean isEmpty() {
		return tasks.darNumeroElementos() == 0;
	}
	
	public int size() {
		return tasks.darNumeroElementos();
	}
	
}
