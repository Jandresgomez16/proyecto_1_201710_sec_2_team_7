package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaDE.CasillaDoble;

public class ListaE<T> implements ILista<T> {

	private Casilla <T> primera;
	private Casilla <T> actualCasilla;
	private Casilla <T> ultima;

	@Override
	public Iterator<T> iterator() {
		return new Iterator <T> (){
			Casilla <T> actual = primera;
			@Override
			public boolean hasNext() {
				if (actual!= null)
					return true;
				else 
					return false;
			}

			@Override
			public T next() {
				T aux = actual.element;
				actual = actual.siguiente;
				return aux;
			}

			@Override
			public void remove() {
				Casilla<T> a = primera;
				Casilla<T> b = null;
				while(a.siguiente != actual) {
					b = a;
					a = a.siguiente;
				}
				if(a == primera) primera = a.siguiente;
				if(b != null) b.siguiente = a.siguiente;
				a.siguiente = null;
			}
		} ;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		Casilla <T> agregada = new Casilla<T>(elem);;
		if (primera == null){
			ultima = primera = agregada;
			actualCasilla = ultima;
		} else {
			ultima.siguiente = agregada;
			ultima = agregada;
		}

	}

	@Override
	public T darElemento(int pos) {
		Casilla <T> actual = primera;
		int posactual = 0 ;
		while (actual != null && posactual < pos)
		{
			actual = actual.siguiente;
			posactual++;
		}
		return actual.element;
	}


	@Override
	public int darNumeroElementos() {
		Casilla <T> actual = primera;
		int numelements = 0;
		while (actual != null){
			numelements++;
			actual = actual.siguiente;
		}
		return numelements;
	}

	public T darElementoPosicionActual() {
		return actualCasilla.element;
	}

	public boolean avanzarSiguientePosicion() {
		if (actualCasilla.siguiente != null){
			actualCasilla = actualCasilla.siguiente;
			return true;
		}
		else 
			return false;
	}

	public boolean retrocederPosicionAnterior() {
		Casilla <T> actual = primera;
		while (actual.siguiente != actualCasilla){
			actual = actual.siguiente;
		}
		if (actual.siguiente == actualCasilla){
			actualCasilla = actual;
			return true;
		}
		else
			return false;
	}

	public static class Casilla <T>
	{
		T element;
		Casilla <T> siguiente;

		public Casilla (T element){
			this.element = element;
		}
		
		public T darElemento() {
			return element;
		}
		
		public void asignarElemento(T e) {
			element = e;
		}

	}

	@Override
	public T eliminarElemento(int pos) {
		if(primera == null) return null;
		Casilla<T> iter = primera;
		Casilla<T> anterior = null;
		while(pos > 0) {
				anterior = iter;
				iter = iter.siguiente;
				if(iter == null) return null;
				pos--;
		}
		T retorno = iter.element;
		if(anterior == null) primera = iter.siguiente; 
		else anterior.siguiente = iter.siguiente;
		if(iter.siguiente == null) ultima = anterior;
		iter.siguiente = null;
		return retorno;
	}

	public static void main(String[] args) {
		ListaE<String> a = new ListaE<>();
		for(int i = 0; i < 100; i++) a.agregarElementoFinal(i + "");
	}

	public T popFirst() {
		if(primera == null) return null;
		T e = primera.element;
		primera = primera.siguiente;
		return e;
	}
}
