package model.data_structures;

public class Stack<T> {
	private ListaDE tasks;
	
	public Stack() {
		tasks = new ListaDE<>();
	}
	
	public void push(T e) {
		tasks.agregarElementoFinal(e);
	}
	
	public T pop() {
		return (T) tasks.popLast();
	}

	public int size() {
		return tasks.darNumeroElementos();
	}
	
	public boolean isEmpty() {
		return tasks.darNumeroElementos() == 0;
	}
}
