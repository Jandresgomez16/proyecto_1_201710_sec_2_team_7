package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaE.Casilla;

public class ListaDE<T> implements ILista<T> {
	private CasillaDoble primera;
	private CasillaDoble ultima;
	private CasillaDoble actual;

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			private CasillaDoble it = primera;

			@Override
			public boolean hasNext() {
				return it != null;
			}

			@Override
			public T next() {
				T e = it.darElemento();
				it = it.next;
				return e;
			}

			@Override
			public void remove() {
				CasillaDoble temp = primera;
				while(temp.next != it) {
					temp = temp.next;
				}
				if(temp == primera) primera = temp.next;  
				else temp.anterior.next = temp.next;
				if(temp.next != null) temp.next.anterior = temp.anterior;
				temp.next = null;
				temp.anterior = null;
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(ultima == null) {
			primera = ultima = actual = new CasillaDoble(elem);

		} else {
			CasillaDoble a = new CasillaDoble(elem);
			ultima.next = a;
			a.anterior = ultima;
			ultima = a;
		}
	}

	@Override
	public T darElemento(int pos) {
		CasillaDoble a = primera;
		while(pos > 0 && a != null) {
			a = a.next; pos--;
		}
		return a == null? null : a.e;
	}


	@Override
	public int darNumeroElementos() {
		int count = 0;
		CasillaDoble a = primera;
		while(a != null) { a = a.next; count++; }
		return count;
	}

	public T darElementoPosicionActual() {
		return actual.darElemento();
	}

	public boolean avanzarSiguientePosicion() {
		if(actual == null) return false;
		if(actual.next == null) return false;
		actual = actual.next;
		return true;
	}

	public boolean retrocederPosicionAnterior() {
		if(actual == null) { actual = primera; return false; }
		if(actual.anterior == null) return false;
		actual = actual.anterior;
		return true;
	}

	public class CasillaDoble {
		private T e;
		private CasillaDoble next;
		private CasillaDoble anterior;
		public CasillaDoble(T elemento) {
			e = elemento;
			anterior = null;
			next = null;
		}

		public T darElemento() {
			return e;
		}

		public void asignarElemento(T elemento) {
			e = elemento;
		}
	}

	@Override
	public T eliminarElemento(int pos) {
		if(primera == null) return null;
		CasillaDoble iter = primera;
		while(pos > 0) {
				iter = iter.next;
				if(iter == null) return null;
				pos--;
		}
		T retorno = iter.e;
		if(iter == primera) primera = iter.next;  
		else iter.anterior.next = iter.next;
		if(iter.next != null) iter.next.anterior = iter.anterior;
		iter.next = null;
		iter.anterior = null;
		return retorno;
	}
	
	public void set (int pos, T element){
		actual = primera;
		int posact = 0;
		while (posact < pos) {
			actual = actual.next;
		posact ++;
		}
		actual.asignarElemento(element);
	}

	public T popLast() {
		if(ultima == null) return null;
		T e = ultima.e;
		ultima = ultima.anterior;
		if(ultima == null) primera = null;
		return e;
	}
}
