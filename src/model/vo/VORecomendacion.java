package model.vo;

public class VORecomendacion {
	long movieID;
	double rating;
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public void setMovieID(long movieID) {
		this.movieID = movieID;
	}
	public long getMovieID() {
		return movieID;
	}
}
