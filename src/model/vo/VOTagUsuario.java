package model.vo;

public class VOTagUsuario extends VOTag {
	private long idUsuario;
	private long idPelicula;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
}
