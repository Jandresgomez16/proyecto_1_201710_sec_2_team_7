package model.vo;

public class VOTag implements Comparable<VOTag>{
	private String tag;
	private long timestamp;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VOTag o) {
		if (this.timestamp < o.timestamp) return -1;
		else if (this.timestamp > o.timestamp) return 1;
		return 0;
	}
	
}
