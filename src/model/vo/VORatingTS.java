package model.vo;

public class VORatingTS extends VORating {

	private long timesTamp;
	
	public long getTimesTamp ()
	{
		return timesTamp;
	}
	public void setTimesTamp (long timesTamp)
	{
		this.timesTamp = timesTamp;
	}
}