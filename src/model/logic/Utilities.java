package model.logic;

import java.util.Comparator;

import model.data_structures.ListaDE;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VORatingTS;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;

public class Utilities<T> {
	public void quickSort ( ListaDE<T> list, int posi, int posf, Comparator<T> c) {
		if(posf <= posi) return;
		int pivot = (int) (Math.random()*(posf - posi)) + posi;
		int i = (pivot == posi? posi + 1 : posi);
		int j = (pivot == posf? posf - 1 : posf); 
		boolean left = true;
		while(i < j) {
			if(left) {
				if(c.compare(list.darElemento(i), list.darElemento(pivot)) > 0) left = !left;
				else i++; 
			} else {
				if(c.compare(list.darElemento(j), list.darElemento(pivot)) <= 0 && pivot != j) {
					T temp = list.darElemento(j);
					list.set(j, list.darElemento(i));
					list.set(i, temp);
					left = !left;
				} else j--;
			}
		}
		if(c.compare(list.darElemento(j), list.darElemento(pivot)) > 0) j = j > pivot? j - 1 : j;
		else if(c.compare(list.darElemento(j), list.darElemento(pivot)) <= 0) j = j > pivot? j : j + 1;

		T temp = list.darElemento(pivot);
		list.set(pivot, list.darElemento(j));
		list.set(j, temp);
		quickSort(list, posi, j - 1, c); quickSort(list, j + 1, posf, c);
	}

	//Por numero de ratings.
	public Comparator<VOPelicula> ordenarA1() {
		return new Comparator<VOPelicula>() {

			@Override
			public int compare(VOPelicula o1, VOPelicula o2) {
				return o1.getNumeroRatings() - o2.getNumeroRatings();
			}
		};
	}

	//Por agno de publicacion, si es igual por promedio de ratings recividos, y si es igual es igual por titulo lexicograficamente.
	public Comparator<VOPelicula> ordenarA2() { 
		return new Comparator<VOPelicula>() {

			@Override
			public int compare(VOPelicula o1, VOPelicula o2) {
				double delta = o1.getAgnoPublicacion() - o2.getAgnoPublicacion();
				if(delta != 0) return delta > 0? 1 : -1;
				delta = o1.getPromedioRatings() - o2.getPromedioRatings();
				if(delta != 0) return delta > 0? 1 : -1;
				return o1.getTitulo().compareTo(o2.getTitulo());	
			}
		};
	}

	//Por rating promedio.
	public Comparator<VOPelicula> ordenarA3() {
		return new Comparator<VOPelicula>() {

			@Override
			public int compare(VOPelicula o1, VOPelicula o2) {
				double key = o1.getPromedioRatings() - o2.getPromedioRatings();
				if(key == 0) return 0;
				return key > 0? 1 : -1;
			}
		};
	}

	//Por agno de publicacion.
	public Comparator<VOPelicula> ordenarA4() { 
		return new Comparator<VOPelicula>() {

			@Override
			public int compare(VOPelicula o1, VOPelicula o2) {
				return o1.getAgnoPublicacion() - o2.getAgnoPublicacion();
			}
		};
	}

	//Por ID de la pelicula
	public Comparator<VOPelicula> ordenar1D(){
		return new Comparator<VOPelicula>(){

			@Override
			public int compare(VOPelicula o1, VOPelicula o2) {
				return (int) (o1.getIdPelicula() - o2.getIdPelicula());
			}
		};
	}

	//Por valor absoluto de rating promedio respecto a un rating.
	public Comparator<VOPelicula> ordenarA5(double ratingBase){
		return new Comparator<VOPelicula>(){
			double rating = ratingBase;
			@Override
			public int compare(VOPelicula o1, VOPelicula o2) {
				double delta = Math.abs(o1.getPromedioRatings() - rating) - Math.abs(o2.getPromedioRatings() - rating);
				if(delta == 0) return 0;
				return delta > 0? 1 : -1;
			}
		};
	}

	//Por timestamp.
	public Comparator<VORatingTS> ordenarA6() { 
		return new Comparator<VORatingTS>() {

			@Override
			public int compare(VORatingTS o1, VORatingTS o2) {
				long key = o1.getTimesTamp() - o2.getTimesTamp();
				if(key == 0) return 0;
				return key > 0? 1 : -1;
			}
		};
	}

	//Por numero de ratings (conteo).
	public Comparator<VOUsuarioConteo> ordenar1B() { 
		return new Comparator<VOUsuarioConteo>() {

			@Override
			public int compare(VOUsuarioConteo o1, VOUsuarioConteo o2) {
				return o1.getConteo() - o2.getConteo();
			}
		};
	}

	//Por timestamp, si es igual por num ratings, y si es igual por ID.
	public Comparator<VOUsuario> ordenar2B() { 
		return new Comparator<VOUsuario>() {
			@Override
			public int compare(VOUsuario o1, VOUsuario o2) {
				long delta = o1.getPrimerTimestamp() - o2.getPrimerTimestamp();
				if(delta != 0) return delta > 0? 1 : -1;
				delta = o1.getNumRatings() - o2.getNumRatings();
				if(delta != 0) return delta > 0? 1 : -1;
				delta = o1.getIdUsuario() - o2.getIdUsuario();
				if(delta == 0) return 0; 
				return delta > 0? 1 : -1;
			}
		};
	}

	//Por diferencia absoluta respectoa  rating
	public Comparator <VOUsuario> ordenar5B(double ratingBase){
		return new Comparator<VOUsuario>() {
			double rating = ratingBase;
			@Override
			public int compare(VOUsuario o1, VOUsuario o2) {
				return 0;
			}


		};	
	}

	//Por timestamp.
	public Comparator<VOTag> ordenar6B() { 
		return new Comparator<VOTag>() {
			@Override
			public int compare(VOTag o1, VOTag o2) {
				long delta = o1.getTimestamp() - o2.getTimestamp();
				if(delta == 0) return 0;
				return delta > 0? 1 : -1;
			}
		};
	}}

