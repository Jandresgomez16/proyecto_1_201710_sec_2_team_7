package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;
import model.data_structures.ListaDE;
import model.data_structures.ListaE;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VORatingTS;
import model.vo.VORecomendacion;
import model.vo.VOTag;
import model.vo.VOTagUsuario;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;

public class ServicioRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {
	private static final String NO_GENRE = "(no genres listed)";
	public ListaDE<VOPelicula> peliculas;
	private ListaDE<VOUsuario> usuarios;
	private ListaDE<VOTagUsuario> tagsUsuario;
	private ListaDE <VORatingTS> ratingsTS;
	private Stack <VOOperacion> operaciones = new Stack<>();

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		long start = System.currentTimeMillis();
		peliculas = new ListaDE<>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaPeliculas)));
			br.readLine();

			String line = br.readLine();
			while(line != null) {
				//[0] movieID, [1] titulo, [2] genre
				String[] items = LectorCSV.parseLine(line);
				VOPelicula p = new VOPelicula();
				int year = LectorCSV.findYear(items[1]);
				p.setAgnoPublicacion(year);

				String titulo = "";
				String[] partes = items[1].split("\\(");
				for(int i = 0; i < partes.length - 1; i++) titulo += partes[i] + "(";
				if(partes.length == 1) titulo = items[1];
				titulo = titulo.substring(0, titulo.length() - 1);
				p.setTitulo(titulo);

				ListaDE<String> generos = new ListaDE<>();
				String[] b = items[2].split("\\|");
				for(String a : b) generos.agregarElementoFinal(a);
				if(b[0].compareTo(NO_GENRE) == 0) generos = new ListaDE<>();
				p.setGenerosAsociados(generos);

				peliculas.agregarElementoFinal(p);

				line = br.readLine();
			} br.close();

		} catch (Exception e) { System.err.println("Error al cargar las peliculas del archivo."); e.printStackTrace(); return false;}

		agregarOperacionSR("cargarPeliculasSR", start, System.currentTimeMillis());
		return true;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		long start = System.currentTimeMillis();
		usuarios = new ListaDE<>();
		ratingsTS = new ListaDE<>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaRatings)));
			br.readLine();

			String line = br.readLine();
			VOUsuario usuarioActual = null;
			while(line != null) {
				//[0] usuarioID, [1] movieID, [2] rating, [3] timestamp
				String[] items = LectorCSV.parseLine(line);
				VORatingTS ratingTimesTamp = new VORatingTS();

				//Revisa si se cambio de usuario o si se sigue con el mismo.
				long idUsuario = Long.parseLong(items[0]);
				if(usuarioActual == null) { usuarioActual = new VOUsuario(); usuarioActual.setIdUsuario(idUsuario); }
				if(usuarioActual.getIdUsuario() != idUsuario) {
					usuarios.agregarElementoFinal(usuarioActual);
					usuarioActual = new VOUsuario();
					usuarioActual.setIdUsuario(idUsuario); 
					usuarioActual.setNumRatings(usuarioActual.getNumRatings() + 1);
					usuarioActual.setPrimerTimestamp(Long.parseLong(items[3]));
				} else { usuarioActual.setNumRatings(usuarioActual.getNumRatings() + 1); }

				//Le suma un rating a la pelicula que le corresponda el rating.
				long movieID = Long.parseLong(items[1]);
				VOPelicula p = buscarPelicula(movieID);
				double vRating = Double.parseDouble(items[2]);
				if(p != null) {
					p.setPromedioRatings((p.getPromedioRatings()*p.getNumeroRatings()) + vRating);
					p.setNumeroRatings(p.getNumeroRatings() + 1);
				}

				ratingTimesTamp.setIdPelicula(movieID);
				ratingTimesTamp.setIdUsuario(idUsuario);
				ratingTimesTamp.setRating(vRating);
				ratingTimesTamp.setTimesTamp(Long.parseLong(items[3]));

				ratingsTS.agregarElementoFinal(ratingTimesTamp);
				line = br.readLine();
			} usuarios.agregarElementoFinal(usuarioActual); br.close();
		} catch (Exception e) { System.err.println("Error al cargar los ratings del archivo."); e.printStackTrace(); return false;}

		agregarOperacionSR("cargarRatingsSR", start, System.currentTimeMillis());
		return true;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		long start = System.currentTimeMillis();
		tagsUsuario = new ListaDE<>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaTags)));
			br.readLine();

			String line = br.readLine();
			while(line != null) {
				//[0] userID, [1] movieID, [2] tag, [3] timestamp
				String[] items = LectorCSV.parseLine(line);
				VOTagUsuario tag = new VOTagUsuario();
				tag.setIdUsuario(Long.parseLong(items[0]));
				tag.setTag(items[2]);
				tag.setTimestamp(Long.parseLong(items[3]));
				tag.setIdPelicula(Long.parseLong(items[1]));

				long movieID = Long.parseLong(items[1]);
				VOPelicula p = buscarPelicula(movieID);
				if(p != null) {
					ILista<String> tags = p.getTagsAsociados();
					if(tags == null) tags = new ListaDE();
					tags.agregarElementoFinal(items[2]);
					p.setTagsAsociados(tags);
				} tagsUsuario.agregarElementoFinal(tag);	
				line = br.readLine();
			}  br.close();
		} catch (Exception e) { System.err.println("Error al cargar los ratings del archivo."); e.printStackTrace(); return false;}

		agregarOperacionSR("cargarTagsSR", start, System.currentTimeMillis());
		return true;
	}

	@Override
	public int sizeMoviesSR() {
		long start = System.currentTimeMillis();
		agregarOperacionSR("sizeMoviesSR", start, System.currentTimeMillis());
		return peliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		long start = System.currentTimeMillis();
		agregarOperacionSR("sizeUsersSR", start, System.currentTimeMillis());
		return usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		long start = System.currentTimeMillis();
		agregarOperacionSR("sizeTagsSR", start, System.currentTimeMillis());
		return tagsUsuario.darNumeroElementos();
	}

	//Metodo A1
	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		long start = System.currentTimeMillis();
		ListaE <VOGeneroPelicula> peliculasPop = new ListaE<>();

		//Se ordenan las peliculas por numero de ratings
		Utilities<VOPelicula> utl = new Utilities<>();
		utl.quickSort(peliculas, 0, peliculas.darNumeroElementos() - 1, utl.ordenarA1());

		ListaDE <VOGeneroPelicula> gp = splitGeneros(peliculas);
		Iterator<VOGeneroPelicula> iter = gp.iterator();
		while (iter.hasNext()){
			VOGeneroPelicula peliculasGenero = iter.next();
			VOGeneroPelicula gPeliculasPop = new VOGeneroPelicula();
			ListaE <VOPelicula> peliculasTemp = (ListaE<VOPelicula>) peliculasGenero.getPeliculas();
			ListaE <VOPelicula> peliculasMasPop = new ListaE<>();
			for (int i = 0; i < n && i < peliculasTemp.darNumeroElementos(); i++)
				peliculasMasPop.agregarElementoFinal(peliculasTemp.darElemento(i));
			gPeliculasPop.setGenero(peliculasGenero.getGenero());
			gPeliculasPop.setPeliculas(peliculasMasPop);
			peliculasPop.agregarElementoFinal(gPeliculasPop);	
		}
		agregarOperacionSR("peliculasPopularesSR", start, System.currentTimeMillis());
		return peliculasPop;
	}

	//Metodo A2
	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		long start = System.currentTimeMillis();

		//Solo ordenar la lista
		Utilities<VOPelicula> utl = new Utilities<>();
		utl.quickSort(peliculas, 0, peliculas.darNumeroElementos() - 1, utl.ordenarA2());

		agregarOperacionSR("catalogoPeliculasOrdenadoSR", start, System.currentTimeMillis());
		return peliculas;
	}

	//Metodo A3
	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		long start = System.currentTimeMillis();
		ILista <VOGeneroPelicula> peliculasPop = new ListaDE<>();

		//Se ordenan las peliculas por promedio de rating
		Utilities<VOPelicula> utl = new Utilities<>();
		utl.quickSort(peliculas, 0, peliculas.darNumeroElementos() - 1, utl.ordenarA3());

		ListaDE <VOGeneroPelicula> gp = splitGeneros(peliculas);
		Iterator<VOGeneroPelicula> iter = gp.iterator();
		while (iter.hasNext()){
			VOGeneroPelicula peliculasGenero = iter.next();
			VOGeneroPelicula gPeliculasPop = new VOGeneroPelicula();
			ListaDE <VOPelicula> peliculasTemp = (ListaDE<VOPelicula>) peliculasGenero.getPeliculas();
			ListaDE <VOPelicula> peliculasMasPop = new ListaDE<>();
			peliculasMasPop.agregarElementoFinal(peliculasTemp.darElemento(0));
			gPeliculasPop.setGenero(peliculasGenero.getGenero());
			gPeliculasPop.setPeliculas(peliculasMasPop);
			peliculasPop.agregarElementoFinal(gPeliculasPop);	
		}
		agregarOperacionSR("recomendarGeneroSR", start, System.currentTimeMillis());
		return peliculasPop;
	}

	//Metodo A4
	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		long start = System.currentTimeMillis();
		//Se ordenan las peliculas por agno de publicacion
		Utilities<VOPelicula> utl = new Utilities<>();
		utl.quickSort(peliculas, 0, peliculas.darNumeroElementos() - 1, utl.ordenarA4());

		agregarOperacionSR("opinionRatingsGeneroSR", start, System.currentTimeMillis());
		return splitGeneros(peliculas);

	}

	//Metodo A5
	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		long start = System.currentTimeMillis();
		ILista<VOPeliculaPelicula> recomendaciones = new ListaE<VOPeliculaPelicula>();
		Queue<VORecomendacion> queue = new Queue();
		cargarRecomendaciones(queue, rutaRecomendacion);

		while(!queue.isEmpty()) {
			VORecomendacion r = queue.dequeue();
			double rating = r.getRating();
			VOPelicula rP = buscarPelicula(r.getMovieID());
			String genero = rP == null ? null : rP.getGenerosAsociados().darElemento(0);


			VOPeliculaPelicula vopp = new VOPeliculaPelicula();
			vopp.setPelicula(rP);
			vopp.setPeliculasRelacionadas(new ListaDE<VOPelicula>());
			recomendaciones.agregarElementoFinal(vopp);

			Iterator<VOPelicula> iter = peliculas.iterator();
			if(genero != null) {
				while(iter.hasNext()) {
					VOPelicula p = iter.next();
					if(p.getPromedioRatings() >= rating - 0.5 && rating + 0.5 >= p.getPromedioRatings()) {
						boolean agregado = false;
						Iterator<String> itrG = p.getGenerosAsociados().iterator();
						while(!agregado && itrG.hasNext()) {
							if(itrG.next().compareTo(genero) == 0) {
								vopp.getPeliculasRelacionadas().agregarElementoFinal(p);
								agregado = true;
							}
						}
					}
				}
			}

			Utilities<VOPelicula> utl = new Utilities<>();
			utl.quickSort((ListaDE<VOPelicula>)vopp.getPeliculasRelacionadas(), 0, vopp.getPeliculasRelacionadas().darNumeroElementos() - 1, utl.ordenarA5(rating));

		}
		agregarOperacionSR("recomendarPeliculasSR", start, System.currentTimeMillis());
		return recomendaciones;
	}

	//Metodo A6
	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		long start = System.currentTimeMillis();
		ListaE <VORating> ratingsPeli = new ListaE<>();

		//Se ordenan los ratingsTimesTamp por mas reciente a menos reciente (por el timesTamp)
		Utilities<VORatingTS> utl = new Utilities<>();
		utl.quickSort(ratingsTS, 0, peliculas.darNumeroElementos() - 1, utl.ordenarA6());

		Iterator iteratorRatings = ratingsTS.iterator();
		while (iteratorRatings.hasNext()){
			VORatingTS ratingsTimes = (VORatingTS) iteratorRatings.next();
			if (ratingsTimes.getIdPelicula() == idPelicula)
				ratingsPeli.agregarElementoFinal(toVORating(ratingsTimes));
		}
		agregarOperacionSR("ratingsPeliculaSR", start, System.currentTimeMillis());
		return ratingsPeli;
	}

	//Metodo 1B
	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		long start = System.currentTimeMillis();
		ListaDE<VOGeneroUsuario> asw = new ListaDE<>();

		Iterator<VORatingTS> itrR = ratingsTS.iterator();
		while(itrR.hasNext()) {
			VORating r = itrR.next();
			VOPelicula p = buscarPelicula(r.getIdPelicula());
			ILista<String> generos = new ListaE<>();
			if(p != null) generos = p.getGenerosAsociados();

			Iterator<String> itrG = generos.iterator();
			while(itrG.hasNext()) {
				String genero = itrG.next();

				boolean hayGenero = false;
				Iterator<VOGeneroUsuario> itrGU = asw.iterator();
				while(itrGU.hasNext() && !hayGenero) {
					VOGeneroUsuario vogu = itrGU.next();  
					if(genero.compareToIgnoreCase(vogu.getGenero()) == 0) {
						hayGenero = true;

						boolean agregado = false;
						Iterator<VOUsuarioConteo> itrU = vogu.getUsuarios().iterator();
						while(!agregado && itrU.hasNext()) {
							VOUsuarioConteo uc = itrU.next();
							if(r.getIdUsuario() == uc.getIdUsuario()) {
								agregado = true;
								uc.setConteo(uc.getConteo() + 1);
							}
						} if(!agregado) {
							VOUsuarioConteo uc = new VOUsuarioConteo();
							uc.setIdUsuario(r.getIdUsuario());
							uc.setConteo(1);
							ILista<VOUsuarioConteo> l = vogu.getUsuarios();
							l.agregarElementoFinal(uc);
						}
					}
				} if(!hayGenero) {
					VOGeneroUsuario vogu = new VOGeneroUsuario();
					vogu.setGenero(genero);
					VOUsuarioConteo uc = new VOUsuarioConteo();
					uc.setIdUsuario(r.getIdUsuario());
					uc.setConteo(1);
					ILista<VOUsuarioConteo> l = new ListaDE<VOUsuarioConteo>();
					l.agregarElementoFinal(uc);
					vogu.setUsuarios(l);
					asw.agregarElementoFinal(vogu);
				}
			}
		}
		filtrarUsuarios(asw, n);
		agregarOperacionSR("usuariosActivosSR", start, System.currentTimeMillis());
		return asw;
	}

	//Metodo 2B
	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		long start = System.currentTimeMillis();

		//Se ordena la lista de usuarios
		Utilities<VOUsuario> utl = new Utilities<>();
		utl.quickSort(usuarios, 0, usuarios.darNumeroElementos() - 1, utl.ordenar2B());

		agregarOperacionSR("catalogoUsuariosOrdenadoSR", start, System.currentTimeMillis());
		return usuarios;
	}

	//Metodo 3B
	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		long start = System.currentTimeMillis();
		ListaDE<VOGeneroPelicula> split = splitGeneros(peliculas);

		Iterator<VOGeneroPelicula> itr = split.iterator();
		while(itr.hasNext()) {
			VOGeneroPelicula gp = itr.next();
			VOPelicula p = null;
			int numTags = 0;

			Iterator<VOPelicula> itrP = gp.getPeliculas().iterator();
			while(itrP.hasNext()) {
				VOPelicula pActual = itrP.next();
				if(pActual.getNumeroTags() >= numTags) p = pActual;
			}
			ListaE<VOPelicula> l = new ListaE<>();
			l.agregarElementoFinal(p);
			gp.setPeliculas(l);
		}

		agregarOperacionSR("recomendarTagsGeneroSR", start, System.currentTimeMillis());
		return split;
	}

	//Metodo 4B
	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		long start = System.currentTimeMillis();
		ListaE<VOUsuarioGenero> retorno = new ListaE<>();

		Iterator<VOUsuario> itrU = usuarios.iterator();
		while(itrU.hasNext()) {
			VOUsuarioGenero ug = new VOUsuarioGenero();
			ListaE<VOGeneroTag> lgt = new ListaE<>();
			long userID = itrU.next().getIdUsuario();
			ug.setIdUsuario(userID);

			Iterator<VOTagUsuario> itrT = tagsUsuario.iterator();
			while(itrT.hasNext()) {
				VOTagUsuario t = itrT.next();
				if(t.getIdUsuario() == userID) {
					VOPelicula p = buscarPelicula(t.getIdPelicula());
					if(p != null) {
						Iterator<String> itrG = p.getGenerosAsociados().iterator();
						while(itrG.hasNext()) {
							String genero = itrG.next();
							boolean hayGenero = false;
							Iterator<VOGeneroTag> itrGT = lgt.iterator();
							while(!hayGenero && itrGT.hasNext()) {
								VOGeneroTag gt = itrGT.next();
								if(gt.getGenero().compareTo(genero) == 0) {
									hayGenero = true;
									ILista l = gt.getTags();
									l.agregarElementoFinal(t.getTag());
								}
							} if(!hayGenero) {
								VOGeneroTag gt = new VOGeneroTag();
								gt.setGenero(genero);
								ILista l = new ListaE<VOTag>();
								l.agregarElementoFinal(t.getTag());
								gt.setTags(l);
								lgt.agregarElementoFinal(gt);
							}
						}
					}
				}
			} ug.setListaGeneroTags(lgt); retorno.agregarElementoFinal(ug);
		} 
		agregarOperacionSR("opinionTagsGeneroSR", start, System.currentTimeMillis());
		return retorno;
	}

	//Metodo 5B
	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		long start = System.currentTimeMillis();
		ILista<VOPeliculaUsuario> recomendaciones = new ListaE<VOPeliculaUsuario>();
		Queue<VORecomendacion> queue = new Queue();
		cargarRecomendaciones(queue, rutaRecomendacion);

		while(!queue.isEmpty()) {
			VORecomendacion r = queue.dequeue();
			double rating = r.getRating();
			long movieID = r.getMovieID();
			VOPeliculaUsuario current = new VOPeliculaUsuario();
			current.setUsuariosRecomendados(new ListaDE<VOUsuario>());
			current.setIdPelicula(movieID);
			recomendaciones.agregarElementoFinal(current);

			Iterator<VORatingTS> itr = ratingsTS.iterator();
			while(itr.hasNext()) {
				VORating voRating = itr.next();
				if(voRating.getIdPelicula() == movieID && (rating - 0.5 <= voRating.getRating() && voRating.getRating() <= rating + 0.5)) {
					Iterator<VOUsuario> itrU = usuarios.iterator();
					long userID = voRating.getIdUsuario();
					boolean agregado = false;
					while(!agregado && itrU.hasNext()) {
						VOUsuario vou = itrU.next();
						if(vou.getIdUsuario() == userID) {
							agregado = true;
							current.getUsuariosRecomendados().agregarElementoFinal(vou);
						}
					}
				}
			}

			Utilities<VOUsuario> utl = new Utilities<>();
			utl.quickSort((ListaDE<VOUsuario>)current.getUsuariosRecomendados(), 0, current.getUsuariosRecomendados().darNumeroElementos() - 1, utl.ordenar5B(rating));
		}

		agregarOperacionSR("recomendarUsuariosSR", start, System.currentTimeMillis());
		return recomendaciones;
	}

	//Metodo 6B
	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		long start = System.currentTimeMillis();
		VOPelicula movie = buscarPelicula(idPelicula);
		ListaDE<VOTag> tagsMovie = new ListaDE<VOTag>();
		if(movie == null) return tagsMovie;
		ILista<String> tagString = movie.getTagsAsociados();

		Iterator<VOTagUsuario> itr = tagsUsuario.iterator();
		while(itr.hasNext()) {
			VOTag t = itr.next();
			String compare = t.getTag();

			Iterator<String> itrS = tagString.iterator(); boolean found = false;
			while(!found && itrS.hasNext()) {
				if(itrS.next().compareTo(compare) == 0) found = true; 
			} if(found) tagsMovie.agregarElementoFinal(t);
		}

		Utilities<VOTag> utl = new Utilities<>();
		utl.quickSort(tagsMovie, 0, tagsMovie.darNumeroElementos() - 1, utl.ordenar6B());

		agregarOperacionSR("tagsPeliculaSR", start, System.currentTimeMillis());
		return tagsMovie;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		VOOperacion nuevOperacion = new VOOperacion();
		nuevOperacion.setOperacion(nomOperacion);
		nuevOperacion.setTimestampInicio(tinicio);
		nuevOperacion.setTimestampFin(tfin);
		operaciones.push(nuevOperacion);
	}

	//Metodo 1C
	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		ListaE<VOOperacion> listaOp = new ListaE<>();
		while (!operaciones.isEmpty())
			listaOp.agregarElementoFinal(operaciones.pop());
		for(int i = listaOp.darNumeroElementos() - 1; i >= 0; i--) 
			operaciones.push(listaOp.darElemento(i));
		return listaOp;
	}

	//Metodo 2C
	@Override
	public void limpiarHistorialOperacionesSR() {
		operaciones = new Stack<VOOperacion>();
	}

	//Metodo 3C
	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		ListaDE<VOOperacion> listaOp = new ListaDE<>();
		int i = 0;
		while (!operaciones.isEmpty() && i < n) {
			listaOp.agregarElementoFinal(operaciones.pop()); i++;
		}

		for(int k = listaOp.darNumeroElementos() - 1; k >= 0; k--) 
			operaciones.push(listaOp.darElemento(i));

		return listaOp;
	}

	//Metodo 4C
	@Override
	public void borrarUltimasOperaciones(int n) {
		while(n > 0) {
			operaciones.pop(); n--;
		}
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		long start = System.currentTimeMillis();

		Utilities<VOPelicula> util = new Utilities<>();
		ListaDE<String> genrs = new ListaDE<>();
		//Se ordenan las peliculas por ID para saber cual es el mayor ID y asi, poder crear uno nuevo
		util.quickSort(peliculas, 0, peliculas.darNumeroElementos()-1, util.ordenar1D());
		long idPelicula = (peliculas.darElemento(peliculas.darNumeroElementos()-1).getIdPelicula() + 1);
		for (int i = 0;i < generos.length; i++)
			genrs.agregarElementoFinal(generos[i]);

		VOPelicula nuevPelicula = new VOPelicula();
		nuevPelicula.setTitulo(titulo);
		nuevPelicula.setAgnoPublicacion(agno);
		nuevPelicula.setGenerosAsociados(genrs);
		nuevPelicula.setIdPelicula(idPelicula);
		nuevPelicula.setNumeroRatings(0);
		nuevPelicula.setNumeroTags(0);
		nuevPelicula.setPromedioRatings(0);
		ListaE<String> tags = new ListaE<>();
		nuevPelicula.setTagsAsociados(tags);
		peliculas.agregarElementoFinal(nuevPelicula);

		agregarOperacionSR("agregarPelicula", start, System.currentTimeMillis());
		return idPelicula;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		long start = System.currentTimeMillis();
		VORatingTS nuevRating = new VORatingTS();
		nuevRating.setIdUsuario(idUsuario);
		nuevRating.setIdPelicula(idPelicula);
		nuevRating.setRating(rating);
		nuevRating.setTimesTamp(Long.MAX_VALUE);
		ratingsTS.agregarElementoFinal(nuevRating);
		agregarOperacionSR("agregarRating", start, System.currentTimeMillis());
	}

	public VOPelicula buscarPelicula(long movieID) {
		Iterator<VOPelicula> itr = peliculas.iterator();
		while(itr.hasNext()) {
			VOPelicula p = itr.next();
			if(p.getIdPelicula() == movieID) return p;
		} return null;
	}

	public VORating toVORating(VORatingTS ratingT)	{
		VORating rating = new VORating();
		rating.setIdPelicula(ratingT.getIdPelicula());
		rating.setIdUsuario(ratingT.getIdUsuario());
		rating.setRating(ratingT.getRating());

		return rating;
	}

	private void filtrarUsuarios(ListaDE<VOGeneroUsuario> listasGeneros, int n) {
		Iterator iterGen = listasGeneros.iterator();
		while (iterGen.hasNext()) {
			ListaDE<VOUsuarioConteo> newUsers = new ListaDE<>();
			VOGeneroUsuario genUser = (VOGeneroUsuario) iterGen.next();
			ListaDE<VOUsuarioConteo> listUsers = (ListaDE<VOUsuarioConteo>) genUser.getUsuarios();

			//Se ordenan los usuarios del genUser segun el conteo de cada uno
			Utilities<VOUsuarioConteo> utl = new Utilities<>();
			utl.quickSort(listUsers, 0, listUsers.darNumeroElementos() - 1, utl.ordenar1B());

			Iterator iterUsers = listUsers.iterator();
			int count = 0;
			while (iterUsers.hasNext() && count < n) {
				newUsers.agregarElementoFinal((VOUsuarioConteo) iterUsers.next()); count++;
			} genUser.setUsuarios(newUsers);
		}
	}

	public ListaDE<VOGeneroPelicula> splitGeneros(ListaDE<VOPelicula> lista) {
		ListaDE<VOGeneroPelicula> split = new ListaDE<VOGeneroPelicula>();

		Iterator<VOPelicula> itr = lista.iterator();
		while(itr.hasNext()) {
			VOPelicula p = itr.next();
			ILista<String> generos = p.getGenerosAsociados();
			Iterator<String> itrGeneros = generos.iterator();
			while(itrGeneros.hasNext()) {
				String genero = itrGeneros.next();
				boolean hayGenero = false;
				Iterator<VOGeneroPelicula> itrGeneroPelicula = split.iterator();
				while(!hayGenero && itrGeneroPelicula.hasNext()) {
					VOGeneroPelicula gp = itrGeneroPelicula.next();
					if(gp.getGenero().equals(genero)) {
						hayGenero = true;
						ListaDE listaGeneroPelicula = (ListaDE) gp.getPeliculas();
						if(listaGeneroPelicula == null) listaGeneroPelicula = new ListaDE<>();
						listaGeneroPelicula.agregarElementoFinal(p);
						gp.setPeliculas(listaGeneroPelicula);
					}
				} if(!hayGenero) { 
					VOGeneroPelicula gp = new VOGeneroPelicula();
					gp.setGenero(genero);
					ListaDE listaGeneroPelicula = new ListaDE<>();
					listaGeneroPelicula.agregarElementoFinal(p);
					gp.setPeliculas(listaGeneroPelicula);
					split.agregarElementoFinal(gp);
				}
			}
		}
		return split;
	}

	private void cargarRecomendaciones(Queue<VORecomendacion> queue, String rutaRecomendacion) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaRecomendacion)));

			String line = br.readLine();
			VORecomendacion r = new VORecomendacion();
			while(line != null) {
				//[0] movieID, [1] rating
				String[] items = LectorCSV.parseLine(line);
				r.setMovieID(Long.parseLong(items[0]));
				r.setRating(Double.parseDouble(items[1]));
				queue.enqueue(r);
				line = br.readLine();
			}  br.close();
		} catch (Exception e) { System.err.println("Error al cargar las recomendaciones del archivo."); e.printStackTrace(); }
	}
}