package model.logic;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class LectorCSV {
	public static final char COMA = ',';
	public static final char COMILLA = '"';

	public static String[] parseLine(String line) {
		if(line == null | line.isEmpty()) return null;
		ArrayList<String> parse = new ArrayList<>();
		char[] chars = line.toCharArray();

		String asw = "";
		boolean enComillas = false;
		boolean enExtraComilla = false;

		for(char a : chars) {
			if(enComillas) {
				if(!enExtraComilla) {
					if(a == '"')
						enExtraComilla = true;
					asw += a;
				} else {
					if(a == ',') {
						parse.add(asw.substring(0, asw.length() - 1));
						asw = "";
						enComillas = false;
						enExtraComilla = false;
					} else {
						enExtraComilla = false;
						asw += a;
					}
				}
			} else {
				if(a == '"')
					enComillas = true;
				else if(a == ',') {
					parse.add(asw);
					asw = "";
				} else
					asw += a;
			}
		}
		parse.add(asw);
		String[] parseArray = new String[parse.size()]; int i = 0;
		for(String a : parse) {
			parseArray[i++] = a;
		}

		return parseArray;
	}
	
	public static int findYear(String line) {
		char[] chars = line.toCharArray();
		String num = "";
		
		boolean enParentesis = false;
		for(char a : chars) {
			if(enParentesis) {
				if(a >= '0' && a <= '9') num += a;
				else if(a == ')') enParentesis = false;
				else { enParentesis = false; num = ""; }
			} else if(a == '(') {
				enParentesis = true;
				num = "";
			}
		}
		int year = num.equals("") ? -1 : Integer.parseInt(num);
		return (year >= 1950 && year <= 2016) ? year : -1;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(new InputStreamReader(System.in));
		String line = scan.nextLine();
		String[] asw = parseLine(line);
		for(String a : asw) System.out.println(a);
		System.out.println(findYear(asw[1]));
	}
}
