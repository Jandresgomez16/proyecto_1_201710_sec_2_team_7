package pruebas;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class TestStack extends TestCase {
	private Stack stack;

	public void setup() {
		stack = new Stack<>();
		for(int i = 0; i < 10; i++ ) stack.push("" + i);
	}
	
	public void testPop() {
		setup();
		for(int i = 0; i < 10; i++ ) assertEquals((9-i) + "", stack.pop());
	}
	
	public void testIsEmpty() {
		stack = new Stack<>();
		assertEquals(true, stack.isEmpty());
		setup();
		assertEquals(false, stack.isEmpty());
	}
	
	public void testSize() {
		stack = new Stack<>();
		assertEquals(0, stack.size());
		setup();
		assertEquals(10, stack.size());
	}
}
