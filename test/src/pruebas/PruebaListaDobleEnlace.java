package pruebas;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.ListaDE;

public class PruebaListaDobleEnlace extends TestCase {
	public final int CASES = 10;
	ListaDE<String> lista;

	public void setup() {
		lista = new ListaDE<>();
		for(int i = 0; i < CASES; i++) {
			lista.agregarElementoFinal("" + i);
		}
	}

	public void testAgregarDarElemento() {
		setup();
		for(int i = 0; i < CASES; i++) assertEquals(i + "", lista.darElemento(i));
	}

	public void testIterator() {
		setup();
		int i = 0;
		for(String a : lista) {
			assertEquals(i + "", a);
			i++;
		}
	}

	public void testSiguienteAnterior() {
		setup();
		for(int i = 0; i < CASES - 1; i++) {
			assertEquals(i + "", lista.darElementoPosicionActual());
			assertEquals(true, lista.avanzarSiguientePosicion());
		}
		for(int i = 0; i < CASES - 1; i++) {
			assertEquals((CASES - i - 1) + "", lista.darElementoPosicionActual());
			assertEquals(true, lista.retrocederPosicionAnterior());
		}
	}
	
	public void testRemove() {
		setup();
		Iterator<String> itr = lista.iterator();
		assertEquals(CASES, lista.darNumeroElementos());
		int i = 0;
		while(itr.hasNext()) {
			String a = itr.next();
			assertEquals(i++ + "", a);
			itr.remove();
		}
		assertEquals(0, lista.darNumeroElementos());
		setup();
		assertEquals(CASES, lista.darNumeroElementos());
	}
	
	public void testEliminarElemento() {
		setup();
		int i = 0;
		while(lista.darNumeroElementos() > 0) assertEquals(i++ + "", lista.eliminarElemento(0));
		assertEquals(0, lista.darNumeroElementos());
		setup();
		assertEquals(CASES, lista.darNumeroElementos());
	}
	
	public void testSize() {
		setup();
		assertEquals(CASES, lista.darNumeroElementos());
	}
}
